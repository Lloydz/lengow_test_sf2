<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="order")
 *
 * @author Anthony Métivier
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idFlux;

    /**
     * @ORM\Column(type="integer")
     */

    private $orderId;

    /**
     * @ORM\Column(type="float")
     */
    private $orderAmount;

    /**
     * @ORM\Column(type="float")
     */
    private $orderCommission;

    /**
     * Get id
     *
     * @return integer 
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFlux
     *
     * @param integer $idFlux
     * @return Order
     */
    public function setIdFlux($idFlux)
    {
        $this->idFlux = $idFlux;

        return $this;
    }

    /**
     * Get idFlux
     *
     * @return integer 
     */
    public function getIdFlux()
    {
        return $this->idFlux;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     * @return Order
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set orderAmount
     *
     * @param float $orderAmount
     * @return Order
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return float 
     */
    public function getOrderAmount()

    {
        return $this->orderAmount;
    }

    /**
     * Set orderCommission
     *
     * @param float $orderCommission
     * @return Order
     */
    public function setOrderCommission($orderCommission)

    {
        $this->orderCommission = $orderCommission;

        return $this;
    }

    /**
     * Get orderCommission
     *
     * @return float 
     */
    public function getOrderCommission()
    {
        return $this->orderCommission;
    }
}
